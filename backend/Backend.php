<?php
/**
 * Created by PhpStorm.
 * User: yusuphwickama
 * Date: 8/7/17
 * Time: 2:31 PM
 */

//mysqli_report(MYSQLI_REPORT_ALL);

class Backend {

    private static $instance;

    /**
     * Backend constructor.
     */
    private function __construct() {
    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Backend();
            return self::$instance;
        } else {
            return self::$instance;
        }
    }

    /* VENUE */

    // Create a new venue
    public function addVenue($name, $uid, $coordinates, $locDesc, $price, $priceStart, $priceEnd,
                             $advPrice, $size, $openTime, $closeTime) {
        $con = $this->getConnection();

        $con->autocommit(false);

        $vid = $this->getToken(8);
        $sql = $con->prepare("INSERT INTO 
              venue (vid,name,uid,location_cord,location_desc,price,price_start,price_end,adv_price,size,opening_time,closing_time) 
              VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

        $sql->bind_param('sssssiiiiiss', $vid, $name, $uid, $coordinates, $locDesc,
            $price, $priceStart, $priceEnd, $advPrice, $size, $openTime, $closeTime);

        if ($sql->execute()) {
            // The query has succeed, save changes.
            echo json_encode(array(
                'venue' => 'added successful'
            ), JSON_PRETTY_PRINT);
            $con->commit();
            $sql->close();

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'venue' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            $con->rollback();
        }
    }


    // GET venue info by venueID

    public function getVenueById($vid) {
        $this->getVenues($vid, "");
    }

    private function getVenues($id, $typeId) {
        $con = $this->getConnection();

        if ($id == "" && $typeId != "") {
            $sql = $con->prepare("SELECT vid,name,venue.uid,ven_desc,location_cord,location_desc,price,price_start,price_end,adv_price,size,opening_time,closing_time,users.first_name 
                                          FROM venue,users WHERE venue.uid = users.uid AND venue.vid IN (SELECT vid FROM venevent WHERE venevent.evid = ?)");
            $sql->bind_param('i', $typeId);
        } elseif ($id != "" && $typeId == "") {
            $sql = $con->prepare("SELECT vid,name,venue.uid,ven_desc,location_cord,location_desc,price,price_start,price_end,adv_price,size,opening_time,closing_time,users.first_name FROM venue,users WHERE venue.uid = users.uid AND vid = ?");
            $sql->bind_param('s', $id);
        } else {
            $sql = $con->prepare("SELECT vid,name,venue.uid,ven_desc,location_cord,location_desc,price,price_start,price_end,adv_price,size,opening_time,closing_time,users.first_name FROM venue,users WHERE venue.uid = users.uid");
        }


        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($vid, $name, $uid, $venDesc, $coord, $desc, $price, $priceStart, $priceEnd, $advPrice, $size, $oTime, $cTime, $ownerName);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'venueId' => $vid,
                    'venueName' => $name,
                    'ownerId' => $uid,
                    'ownerName' => $ownerName,
                    'venueDescription' => $venDesc,
                    'coordinates' => $coord,
                    'locationDesc' => $desc,
                    'price' => $price,
                    'priceStart' => $priceStart,
                    'priceEnd' => $priceEnd,
                    'advancePrice' => $advPrice,
                    'accumulation' => $size,
                    'openingTime' => $oTime,
                    'closingTime' => $cTime,
                    'images' => $this->getVenuePhotos($vid),
                    'bookings' => $this->getVenueBookings($vid)
                ));
            }

            $sql->close();

            echo json_encode($results, JSON_PRETTY_PRINT);

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'account' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }
    }


    // GET All venues

    private function getVenuePhotos($vid) {
        $con = $this->getConnection();
        $sql = $con->prepare("SELECT url FROM photos WHERE vid = ? ");

        $sql->bind_param('s', $vid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($imageUrl);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array('venueId' => $vid, 'image' => $imageUrl));
            }

            return $results;

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'photos' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }

        return array();
    }

    //UPDATE Venue info

    private function getVenueBookings($vid) {
        $con = $this->getConnection();
        $sql = $con->prepare("SELECT date FROM bookings WHERE vid = ? ");

        $sql->bind_param('s', $vid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($date);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array('venueId' => $vid, 'date' => $date));
            }

            $sql->close();

            return $results;

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'bookings' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }

        return array();
    }


    public function getVenuesByType($typeId) {
        $this->getVenues("", $typeId);
    }

    public function getAllVenues() {
        $this->getVenues("", "");
    }

    public function updateVenue($vid, $name, $uid, $coordinates, $locDesc, $price, $priceStart, $priceEnd,
                                $advPrice, $size, $openTime, $closeTime) {
        $con = $this->getConnection();

        $con->autocommit(false);

        $sql = $con->prepare("UPDATE venue SET  name=?,uid=?,location_cord=?,location_desc=?,price=?,price_start=?,
                              price_end=?,adv_price=?,size=?,opening_time=?,closing_time=? WHERE vid = ?");

        $sql->bind_param('ssssffffisss', $name, $uid, $coordinates, $locDesc, $price, $priceStart, $priceEnd, $advPrice, $size, $openTime, $closeTime, $vid);

        if ($sql->execute()) {
            // The query has succeed, save changes.
            $con->commit();
            $sql->close();

            echo json_encode(array(
                'venue' => 'updated successful'
            ), JSON_PRETTY_PRINT);

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'venue' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            $con->rollback();
        }

    }

    /* END of VENUE */


    /* SUGGESTONS */

    public function getSuggestionByEventId($evId, $query) {
        $this->getSuggestions($evId, $query);
    }

    private function getSuggestions($evId, $query) {
        $con = $this->getConnection();

        if ($evId != "") {
            $sql = $con->prepare("SELECT DISTINCT vid,name,location_desc FROM venue WHERE venue.vid IN (SELECT vid FROM venevent WHERE venevent.evid = ?) AND ( name LIKE CONCAT('%',?,'%') OR ven_desc LIKE CONCAT('%',?,'%') OR venue.location_desc LIKE CONCAT('%',?,'%')) LIMIT 15");
            $sql->bind_param('isss', $evId, $query, $query, $query);
        } else {
            $sql = $con->prepare("SELECT DISTINCT vid,name,location_desc FROM venue WHERE name LIKE CONCAT('%',?,'%') OR ven_desc LIKE CONCAT('%',?,'%') OR venue.location_desc LIKE CONCAT('%',?,'%') LIMIT 15");
            $sql->bind_param('sss', $query, $query, $query);
        }


        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($vid, $name, $location);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'venueId' => $vid,
                    'venueName' => $name,
                    'venueLocation' => $location
                ));
            }

            echo json_encode($results, JSON_PRETTY_PRINT);

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'suggest' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }
    }

    public function getGeneralSuggestion($query) {
        $this->getSuggestions("", $query);
    }

    /* END OF SUGGESTONS */

    /* BOOKING Management */

    public function addBooking($uid, $vid, $date) {
        $venPrice = $this->getVenueAmount($vid);

        $initialPrice = round(0.15 * $venPrice, -3);
        $serviceFee = round(0.005 * $venPrice, -3);

        $amount = $initialPrice + $serviceFee;

        $con = $this->getConnection();

        $con->autocommit(false);

        $bid = $this->getToken(8);
        $pid = $this->getToken(8);
        $ppid = $this->getToken(8);

        $sql = $con->prepare("INSERT INTO bookings (bid, uid, vid,amount, date) VALUES (?,?,?,?,?)");
        $sql->bind_param('sssis', $bid, $uid, $vid, $amount, $date);

        // Insert booking
        if ($sql->execute()) {

            $sql = $con->prepare("INSERT INTO payments (pid, uid, bid, service_fee,total_amount) VALUES (?,?,?,?,?)");
            $sql->bind_param('sssii', $pid, $uid, $bid, $serviceFee, $amount);

            // Insert payment
            if ($sql->execute()) {

                $sql = $con->prepare("INSERT INTO pesapal (ppid, uid, pid) VALUES (?,?,?)");
                $sql->bind_param('sss', $ppid, $uid, $pid);

                // Insert pesapal
                if ($sql->execute()) {
                    echo json_encode(array(
                        'booking' => 'added successful',
                        'bookingId' => $bid,
                        'userId' => $uid
                    ), JSON_PRETTY_PRINT);

                    $con->commit();
                    $sql->close();
                } else {
                    echo json_encode(array(
                        'pesapal' => 'failed',
                        'error' => $sql->error
                    ), JSON_PRETTY_PRINT);
                    $con->rollback();
                }

            } else {
                echo json_encode(array(
                    'payments' => 'failed',
                    'error' => $sql->error
                ), JSON_PRETTY_PRINT);
                $con->rollback();
            }

        } else {
            echo json_encode(array(
                'booking' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
            $con->rollback();

        }


    }

    public function getBookingAmount($bid) {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT total_amount FROM payments WHERE bid = ? LIMIT 1");
        $sql->bind_param('s', $bid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($amount);

            $sql->fetch();
            $sql->close();


            return $amount;
        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'booking' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            return 0;
        }
    }

    private function getVenueAmount($vid) {
        $con = $this->getConnection();
        $sql = $con->prepare("SELECT adv_price FROM venue WHERE vid = ? LIMIT 1");
        $sql->bind_param('s', $vid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($amount);
            $sql->fetch();

            $sql->close();

            return $amount;
        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'booking' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            return 0.00;
        }
    }


    /* End of BOOKING Management */


    /* PAYMENTS Management */

    /*public function addPayment($pid, $uid, $bid,$total_amount, $fee){
        $con = $this->getConnection();
        $con->autocommit(false);

        $sql = $con->prepare("");

        $sql->bind_param('',);

        if($sql->execute()){
            $con->commit();
        } else {
            echo json_encode(array(
                'payment' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
            $con->rollback();
        }
    }*/

    public function getPaymentByBooking($bid) {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT pid FROM payments WHERE bid = ? LIMIT 1");
        $sql->bind_param('s', $bid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($pid);
            $sql->fetch();

            return $pid;
        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'payment' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            return '';
        }
    }

    public function updatePaymentStatus($pid, $sid, $trans_ID) {
        $con = $this->getConnection();
        $con->autocommit(false);

        $sql = $con->prepare("UPDATE payments SET status=? WHERE pid=?; UPDATE pesapal SET transc_id=? WHERE pid=?");

        $sql->bind_param('ssss', $sid, $pid, $trans_ID, $pid);

        if ($sql->execute()) {
            $con->commit();
        } else {
            $con->rollback();
        }
    }

    /* End of PAYMENTS Management */


    /* USER Management */

    public function addAuthDetails($uid, $username, $pass) {
        $con = $this->getConnection();

        $pass = password_hash($pass, PASSWORD_DEFAULT);

        $con->autocommit(false);

        $sql = $con->prepare("INSERT INTO authentication (uid, username, password) VALUES(?,?,?)");
        $sql->bind_param('sss', $uid, $username, $pass);

        if ($sql->execute()) {
            $con->commit();
        } else {
            $con->rollback();
        }
    }

    public function verifyUser($uid) {
        $user = $this->getUserById($uid);

    }

    // Create a new user
    public function addUser($fName, $lName, $email, $phoneNum, $role) {
        $con = $this->getConnection();

        $con->autocommit(false);

        $uid = $this->getToken(8);
        $sql = $con->prepare("INSERT INTO users (uid,first_name,last_name,email,
                            phone_number,role_id) VALUES (?,?,?,?,?,?)");

        $sql->bind_param('sssssi', $uid, $fName, $lName, $email, $phoneNum, $role);

        if ($sql->execute()) {
            // The query has succeed, save changes.
            echo json_encode(array(
                'account' => 'added successful'
            ), JSON_PRETTY_PRINT);
            $con->commit();
        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'account' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
            $con->rollback();
        }
    }

    // Get USER info by using their UID
    public function getUserById($uid) {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT first_name,last_name,email,phone_number,roles.role FROM users,roles WHERE uid = ? AND rid = users.role_id LIMIT 1");
        $sql->bind_param('s', $uid);

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($fName, $lName, $email, $phoneNum, $role);

            $sql->fetch();

            $results = array(
                'firstName' => $fName,
                'lastName' => $fName,
                'email' => $email,
                'phoneNumber' => $phoneNum,
                'userRole' => $role
            );

            //echo json_encode($results, JSON_PRETTY_PRINT);

            return $results;

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'account' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);

            return array();
        }
    }

    // Get a list of all users

    public function getUsers() {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT uid,full_name,email,phone_number,roles.role,created_at,modified_at FROM users,roles WHERE rid = users.role_id");

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($uid, $name, $email, $phoneNum, $role, $cAt, $mAt);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'userID' => $uid,
                    'fullName' => $name,
                    'email' => $email,
                    'phoneNumber' => $phoneNum,
                    'userRole' => $role,
                    'joined' => $cAt,
                    'updated' => $mAt
                ));
            }

            echo json_encode($results, JSON_PRETTY_PRINT);

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'account' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }
    }


    /* END of USER Management */

    /* EVENTS Management*/

    // Get a list of all events
    public function getEvents() {
        $con = $this->getConnection();

        $sql = $con->prepare("SELECT evId,event_name,event_type,event_photo FROM events");

        if ($sql->execute()) {
            // The query has succeed, start retrieving data
            $sql->store_result();
            $sql->bind_result($evid, $name, $type, $photo);

            $results = array();

            while ($sql->fetch()) {
                array_push($results, array(
                    'evId' => $evid,
                    'eventName' => $name,
                    'eventType' => $type,
                    'eventImage' => $photo
                ));
            }

            echo json_encode($results, JSON_PRETTY_PRINT);

        } else {
            // The query has failed, undo changes.
            echo json_encode(array(
                'events' => 'failed',
                'error' => $sql->error
            ), JSON_PRETTY_PRINT);
        }
    }

    /* END of EVENTS Management*/


    private function getConnection() {
        $host = 'localhost';
        $user = 'phpmyadmin';
        $pass = ' ';
        $dbname = 'kumbizetu';

        //TODO uncomment before upload


        $con = mysqli_connect($host, $user, $pass, $dbname) or die(json_encode(array(
            'error' => 'Failed to connect to the database.'
        ), JSON_PRETTY_PRINT));

        //$con->query("SET time_zone = \"+03:00\"");

        return $con;
    }

    public function getToken($length) {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet .= "1234567890";
        $max = strlen($codeAlphabet);
        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->secure_rand(0, $max - 1)];
        }
        return $token;
    }

    private function secure_rand($min, $max) {
        $range = $max - $min;
        if ($range < 1) return $min;
        $log = ceil(log($range, 2));
        $bytes = (int)($log / 8) + 1;
        $bit = (int)$log + 1;
        $filter = (int)(1 << $bit) - 1;
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter;
        } while ($rnd > $range);
        return $min + $rnd;
    }


}
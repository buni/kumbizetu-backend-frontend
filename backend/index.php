<?php
/**
 * Created by PhpStorm.
 * User: yusuphwickama
 * Date: 8/7/17
 * Time: 3:11 PM
 */

require_once 'Backend.php';
header('Content-type: application/json');


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $action = $_POST['action'];

    switch ($action) {
        case "U-ID":
            getUserById();
            break;
        case "V-ID":
            getVenueById();
            break;
        case "ALL-V":
            getAllVenues();
            break;
        case "V-EV":
            getVenuesByEvent();
            break;
        case "ALL-U":
            Backend::getInstance()->getUsers();
            break;
        case "ADD-U":
            addUser();
            break;
        case "ADD-BK":
            addBooking();
            break;
        case "ADD-V":
            addVenue();
            break;
        case "SUG-V":
            getSuggestions();
            break;
        case "SUG-VE":
            getSuggestionsByEvent();
            break;
        case "ALL-E":
            getEvents();
            break;
        default:
            echo json_encode(array(
                'error' => 'No action provided'
            ), JSON_PRETTY_PRINT);
            break;

    }
}

function getEvents() {
    Backend::getInstance()->getEvents();
}

function getUserById() {
    $uid = $_POST['uid'];

    $results = Backend::getInstance()->getUserById($uid);

    echo json_encode($results, JSON_PRETTY_PRINT);
}

function getSuggestions() {
    $query = $_POST['query'];

    Backend::getInstance()->getGeneralSuggestion($query);
}

function getSuggestionsByEvent() {
    $query = $_POST['query'];
    $eventId = $_POST['evid'];

    Backend::getInstance()->getSuggestionByEventId($eventId, $query);
}

function getVenueById() {
    $vid = $_POST['vid'];

    Backend::getInstance()->getVenueById($vid);
}

function addBooking() {
    $vid = $_POST['vid'];
    $uid = $_POST['uid'];
    $date = $_POST['bookingDate'];

    Backend::getInstance()->addBooking($uid, $vid, $date);
}

function getAllVenues() {
    Backend::getInstance()->getAllVenues();
}

function getVenuesById() {
    $vid = $_POST['vid'];
    Backend::getInstance()->getVenueById($vid);
}

function getVenuesByEvent() {
    $eventId = $_POST['evid'];
    Backend::getInstance()->getVenuesByType($eventId);
}

function addUser() {
    $fullName = $_POST['fName'];
    $email = $_POST['email'];
    $phoneNum = $_POST['phoneNum'];
    $role = 2;

    Backend::getInstance()->addUser($fullName, $email, $phoneNum, $role);
}

function addVenue() {
    $uid = $_POST['uid'];
    $name = $_POST['venName'];
    $locationCoordinates = $_POST['coords'];
    $loc_desc = $_POST['desc'];
    $price = $_POST['price'];
    $size = $_POST['size'];
    $advPrice = $_POST['advprice'];
    $openTime = $_POST['otime'];
    $closeTime = $_POST['ctime'];
    $startPrice = $_POST['priceStart'];
    $endPrice = $_POST['priceEnd'];

    Backend::getInstance()->addVenue($name, $uid, $locationCoordinates, $loc_desc, $price, $startPrice, $endPrice, $advPrice, $size, $openTime, $closeTime);

}

